package require msgcat

namespace eval TkPieChart {
  array set widgets {
    toplevel,counter 0
  }
}

proc TkPieChart::Plot { values args } {
  variable widgets

  array set opts {
    -windowtitle "Pie Chart"
    -widget ""
    -x 0 
    -y 0
    -aspercent 0
    -titlefont fixed
    -titleoffset 6 
    -selectable 1
  }
  array set opts $args
  if { $opts(-widget) eq "" } {
    if { [ llength [ info command ::project::CreateForm ] ] } {
      set mdi [ ::project::CreateForm \
                    -title [ msgcat::mc $opts(-windowtitle) ] \
                    -type TkPieChart -iniconfig TkPieChart ]
      set w [ $mdi getframe ]
    } else {
      set id [ incr widgets(toplevel,counter) ]
      set w .t$id
      destroy $w
      toplevel $w
      wm geometry $w 700x600
      wm title $w [ msgcat::mc $opts(-windowtitle) ]
    }
    set c [ canvas $w.c ]
    grid $c -row 0 -column 0 -sticky "snew"
    grid rowconfigure $w 0 -weight 1
    grid columnconfigure $w 0 -weight 1
  } else {
    set c $opts(-widget)
  }
  set labeler [ stooop::new pieBoxLabeler $c -justify center -offset 10]
  set x $opts(-x)
  set y $opts(-y)
  set aspercent $opts(-aspercent)
  if { [ info exists opts(-size) ] } {
    set opt_width "-width [ expr { round( $opts(-size) ) } ]"
  } else {
    set opt_width ""
  }
  if { [ info exists opts(-title) ] } {
    set opt_title "-title [ list [ msgcat::mc $opts(-title) ] ]"
  } else {
    set opt_title ""
  }
  foreach o { -windowtitle -title -widget -x -y -size -aspercent } {
    array unset opts $o
  }
  set pie [ eval stooop::new pie $c $x $y \
                -labeler $labeler [ array get opts ] $opt_width $opt_title ]
  set total 0
  foreach v $values {
    foreach {l x} $v break
    set total [ expr { $total + $x } ]
  }
  foreach v $values {
    foreach {l x} $v break
    set slice [ pie::newSlice $pie [ msgcat::mc $l ] ]
    set p [ expr { double($x) / $total } ]
    if { $aspercent } {
      set l "[ expr { $p * 100 } ]%"
    } else {
      set l $x
    }
    pie::sizeSlice $pie $slice $p $l
  }
  $c bind pie($pie) <ButtonPress-1> {
    set TkPieChart::xLast %x
    set TkPieChart::yLast %y
  }
  $c bind pie($pie) <Button1-Motion> [ string map [ list %p $pie ] {
    %W move pie(%p) \
        [ expr {%x - $TkPieChart::xLast}] [expr %y - $TkPieChart::yLast]
    set TkPieChart::xLast %x
    set TkPieChart::yLast %y
  } ]
  set arcs {}
  foreach a [ $c find withtag pie($pie)] {
    if { [ $c type $a ] eq "arc" } {
      lappend arcs $a
    } 
  }
  foreach {xg1 yg1 xg2 yg2} [ $c bbox pie($pie) ] break
  foreach {xa1 ya1 xa2 ya2} [ eval $c bbox $arcs ] break

  set dx [ expr { $xg2 - $xg1 } ]
  set dy [ expr { $yg2 - $yg1 } ]
  set dya [ expr { $ya2 - $ya1 } ]
  switched::configure $pie -width $dx -height [ expr { $dy - $dya + $dx } ]

  return $c
}
