source init.tcl

destroy .c
canvas .c

set plb [new pieBoxLabeler .c -justify center -offset 10]
set pie1 [new pie\
    .c 100 100 -height 200 -thickness 0 -background gray\
    -labeler $plb \
    -title "this is pie #1" -titlefont fixed -titleoffset 6 -selectable 1\
]

set slice11 [pie::newSlice $pie1]
set slice12 [pie::newSlice $pie1]
set slice13 [pie::newSlice $pie1]
set slice14 [pie::newSlice $pie1 {some text}]

foreach slice { slice11 slice12 slice13 slice14 } {
  pie::sizeSlice $::pie1 [set $slice] 0.25
}

foreach {xp1 yp1 xp2 yp2} [ .c bbox pie($pie1) ] break
foreach {xl1 yl1 xl2 yl2} [ .c bbox pieLabeler($plb) ] break

set dx [ expr { $xp2 - $xp1 } ]
set dy1 [ expr { $yl2 - $yl1 } ]

switched::configure $pie1 -width $dx -height [ expr { $dx + $dy1 } ]
pack .c -fill both -expand yes
