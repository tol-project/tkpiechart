lappend auto_path \
    [ file join [ file normalize [ file dir [ info script ] ] ] .. lib ]

package require Tk
package require stooop
package require switched
package require tkpiechart

set _cwd [ file normalize [ file dir [ info script ] ] ]
puts "cargando [ file join $_cwd piechart.tcl ]"
source [ file join $_cwd piechart.tcl ]