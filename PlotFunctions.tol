Text _.autodoc.member.Plot =
  "#!NB; Plot a set of values on a pie chart. The name of the values is taken as the labels of slices. Returns the name of the chart which could be used to add more charts, see option -widget. The argument options, used to control de configuration of the chart, is a Set with an even number of elements where the odd position specify the name of the option and the next position the value. The following are the valid options:

* -windowtitle text : set the title of the window
* -title text : header of the pie
* -x pos : horizontal position, the (0,0) is the top left corner
* -y pos : vertical position, the (0,0) is the top left corner
* -size number: size of the pie in pixels
* -widget w: specify and already created pie chart w is the value returned by this method.
* -aspercent 0|1: show slice values as percent or not

Example:

#pre
#Require TkPlotChart;

Set Values = {
  [[ Real A = 1, Real B = 2, Real C = 2, Real D = 3 ]]
};

Text w = TkPieChart::Plot( Values,
                           [[
                             \"-windowtitle\", \"Window Title Test01\",
                             \"-title\", \"Test 01\",
                             \"-size\", 300
                             ]] )
#unpre 
";
Text Plot( Set values, Set options )
{
  Set _values = For( 1, Card(values), Set( Real k ) {
      Set [[ Name( values[k] ), values[k] ]]
    } );
  Set tclScript = [[ "TkPieChart::Plot", _values ]] << options;
  Set tcl = Tcl_EvalEx( tclScript );
  Real If( Not( tcl[ "status" ] ), {
      Warning( "TkPieChart::Plot FAIL: " + tcl[ "result" ] )
    } );
  Text tcl[ "result" ]
};
